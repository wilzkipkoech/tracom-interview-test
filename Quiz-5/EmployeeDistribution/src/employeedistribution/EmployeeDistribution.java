/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeedistribution;

/**
 *
 * @author willy
 */

import java.math.BigDecimal;
import java.util.*;

public class EmployeeDistribution {
    BigDecimal totalMoney = new BigDecimal("4000.00").setScale(2, BigDecimal.ROUND_HALF_EVEN);


    final static String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static List<String> identifiers = new ArrayList<>();
    final static java.util.Random rand = new java.util.Random();


    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            String fullName = randomIdentifier() + " " + randomIdentifier();
            identifiers.add(fullName);
        }
        Collections.sort(identifiers);
        int employeesPaid = 0;

        BigDecimal paidAmt = new BigDecimal("10.00").setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal remainingAmt = new BigDecimal("4000.00").setScale(2, BigDecimal.ROUND_HALF_EVEN);

        for(int j = 0; j < identifiers.size(); j++) {
            BigDecimal employeePaidAmt = null;
            if (j == 0) {
                employeePaidAmt = paidAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            } else {
                paidAmt = paidAmt.multiply(new BigDecimal("1.20").setScale(2, BigDecimal.ROUND_HALF_EVEN));
                employeePaidAmt = paidAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            }
            System.out.println(identifiers.get(j) + " received Ksh" + employeePaidAmt);

            if (remainingAmt.subtract(employeePaidAmt).compareTo(BigDecimal.ZERO) < 0) {
                break;
            } else {
                remainingAmt = remainingAmt.subtract(employeePaidAmt);
                employeesPaid += 1;
            }


        }

        System.out.println("\nPaid Employees: " + employeesPaid);
        System.out.println("Remaining Amount: " + remainingAmt);

    }

    public static String randomIdentifier() {
        StringBuilder builder = new StringBuilder();
        while(builder.toString().length() == 0) {
            int length = rand.nextInt(5)+5;
            for(int i = 0; i < length; i++) {
                builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
            }
        }
        return builder.toString();
    }
}
