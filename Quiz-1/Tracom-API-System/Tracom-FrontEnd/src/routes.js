import React from 'react';



const Tables = React.lazy(() => import('./views/Base/Tables'));
const Dashboard = React.lazy(() => import('./views/Dashboard/Dashboard.js'));
const Colors = React.lazy(() => import('./views/Theme'));

const Buttons = React.lazy(() => import('./views/Buttons/Buttons'));

const Login = React.lazy(() => import('./views/Pages/Login'));
const Register = React.lazy(() => import('./views/Pages/Register'));



// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },


  { path: '/login', name: 'Log In', component:Login },

{ path: '/buttons/buttons', name: 'Buttons', component: Buttons },

  { path: '/base/tables', name: 'Tables', component: Tables },


];

export default routes;
