/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coinnotes;

 
/**
 *
 * @author willy
 */
import java.util.*;
 
class CoinNotes
{
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        int n = 0;
     int  val=0;
        String string="";
         
        //Scan Total number of Coins and Notes
        //given by client
        System.out.println("Enter Number of times Coins and Notes");
            n=sc.nextInt();

        //A bag to store Coins
        Bag<Coin> bagOfCoins=new Bag<Coin>();
        //A bag to store Notes
        Bag<Note> bagOfNotes=new Bag<Note>();
        for(int i=0;i<n;i++)
        {
            //Scan whether its a Coin or a Note
             System.out.println("Enter Denomination");
            string=sc.next();
           
            //Scan the denomination of the Coin/Note
            val=sc.nextInt();
            switch(string)
            {
                case  "Coin":
                    Coin coin=new Coin();
                    //Set the value of Coin and add it to the bag
                    coin.setValue(val);
                    bagOfCoins.add(coin);
                    break;
                case  "Note":
                    Note note=new Note();
                    //Set the value of Note and add it to the bag
                    note.setValue(val);
                    bagOfNotes.add(note);
                    break;
            }
        }
        System.out.println("Coins :");
        bagOfCoins.display();
        System.out.println("Notes :");
        bagOfNotes.display();
    }
}
 
class Type
{
    int val;
 
    public void setValue(int val) {
        this.val = val;
    }
}
 
class Coin extends Type
{}
 
class Note extends Type
{}
 
class Bag<C extends Type> {
    List<Type> types = new ArrayList<Type>();
 
    public void add(Type t) {
        types.add(t);
    }
 
    public void display() {
        for(Type type : types) {
            System.out.println(type.val);
        }
    }
 
}