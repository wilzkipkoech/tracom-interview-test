import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {Redirect} from 'react-router-dom';
import {PostData} from './PostData';
import { Formik,  Field } from "formik";

class Login extends Component {
  constructor(){
    super();

    this.state = {
     username: '',
     password: '',
     redirectToReferrer: false
    };

    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);

  }



  login() {
    if(this.state.username && this.state.password){
      PostData('login',this.state).then((result) => {
       let responseJson = result;
       if(responseJson.userData){
         sessionStorage.setItem('userData',JSON.stringify(responseJson));
         this.setState({redirectToReferrer: true});
       }

      });
    }

   }

  onChange(e){
    this.setState({[e.target.name]:e.target.value});
   }


  render() {


      if (this.state.redirectToReferrer) {
       return (<Redirect to={'/dashboard'}/>)
     }

     if(sessionStorage.getItem('userData')){
       return (<Redirect to={'/dashboard'}/>)
     }
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Formik>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>

            <fieldset className="form-group">
              <label>E-mail</label>
              <Field
                className="form-control"
                type="email"
                name="username"
                required

                onChange={this.onChange}
                placeholder="Email Here"
              />
            </fieldset>
            <fieldset>
              <label>Password</label>
              <Field
                className="form-control"
                type="password"
                name="password"
                required

                onChange={this.onChange}
                placeholder="Password"

              />
              <br></br>
            </fieldset>
                      <Row>
                        <Col xs="6">
                    
                          <Button  type="submit" color="primary" onClick={this.login} className="px-4">Login</Button>
                        </Col>

                      </Row>
                    </Form>
                    </Formik>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Tracom Services Limited is a limited liability company
                         incorporated in Kenya. We have two Research and Development centres,
                          one in Tanzania and another in Kenya.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
