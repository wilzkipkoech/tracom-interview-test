import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from "./Dashboard";
import AddProduct from "./AddProduct";
import EditProduct from "./EditProduct";

class  Productindex extends Component {
  render() {
    return (
      <Router>
        <Switch>
        {/* <Route path="/Login" exact component={Login} /> */}
          <Route path="/" exact component={Dashboard} />
          <Route path="/AddProduct" exact component={AddProduct} />
          <Route path="/EditProduct/:id" exact component={EditProduct} />
          <Route path="/BackBookList" exact component={Dashboard} />
        </Switch>
      </Router>
    );
  }
}

export default Productindex;
