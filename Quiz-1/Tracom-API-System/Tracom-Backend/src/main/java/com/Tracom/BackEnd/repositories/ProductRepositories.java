package com.Tracom.BackEnd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Tracom.BackEnd.entities.Product;

public interface ProductRepositories extends JpaRepository<Product, String> {

	Product findByproductId(String id);

}
