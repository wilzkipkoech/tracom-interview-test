import React from "react";
import axios from "axios";
import "@fortawesome/fontawesome-free/css/all.min.css";

class ListProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = { products: [], name: null, query: "" };
    this.deleteProduct = this.deleteProduct.bind(this);
    this.refreshProduct = this.refreshProduct.bind(this);
    this.routeAddProduct = this.routeAddProduct.bind(this);
  }

  //REDIRECT PAGE
  routeAddProduct() {
    let path = `/AddProduct`;
    this.props.history.push(path);
  }

  componentDidMount() {
    axios.get("http://localhost:8080/product/all").then(response => {
      this.setState({ products: response.data });
      //console.table(response.data);
      console.warn("products Service is working");
    });

    // CALLING REFRESH Product METHOD
    this.refreshProduct();
  }

  //REFRESH Product METHOD
  refreshProduct() {
    axios.get("http://localhost:8080/product/all").then(response => {
      console.warn("Refresh Service is working");
      this.setState({ products: response.data });
    });
  }
  /*END OF REFRESH METHOD */

  //Route Edit Product
  routeEditProduct(id) {
    // let pathedit = `/EditProduct`;
    // this.props.history.push(pathedit);
    this.props.history.push(`/EditProduct/${id}`);
  }

  //DELETE-METHOD 1 = WORKING
  deleteProduct(id) {
    axios
      .delete("http://localhost:8080/delete/product/" + id)
      .then(response => {
        console.warn("Delete Service is working");
        this.refreshProduct(response);

     
      });
  }
  /*END OF DELETE METHOD = 1*/

  render() {
    return (
      <div className="col-sm-12">
        <br />

        <h3 align="center">List Products</h3>
        <br />
        <div className="container" onLoad={this.refreshProduct}>
          <button
            className="btn btn-success"
            type="submit"
            onClick={this.routeAddProduct}
          >
            <i className="fa fa-plus"> Add Products</i>
          </button>
          <br />

          <br />

          <table className="table">
            <thead>
              <tr>

                <th>Product Name</th>
                <th>Product Code</th>
                <th>Product Category</th>
                <th>Product Description</th>
                 <th>Product Price</th>


                <th> &nbsp; &nbsp; &nbsp; &nbsp;ACTION</th>
              </tr>
            </thead>
            <tbody>
              {this.state.products.map(product => (
                <tr key={product.productId}>
                  <td>{product.productName}</td>
                  <td>{product.productCode}</td>
                  <td>{product.productCategory}</td>
                  <td>{product.productDescription}</td>
                  <td>{product.productPrice}</td>
                  <td>
                    <button className="btn btn-primary" type="submit">
                      <i
                        className="fa fa-edit"
                        onClick={() => this.routeEditProduct(product.productId)}
                      >
                        Edit
                      </i>
                    </button>
                    &nbsp;
                    <button
                      className="btn btn-danger"
                      //NORMAL CALL
                      // onClick={() => this.deleteProduct(Product.ProductId)}

                      //CALL WITH CONFIRM MESSAGE
                      onClick={() =>
                        window.confirm(
                          "Are you sure you wish to delete this Product? "
                        ) && this.deleteProduct(product.productId)
                      }
                    >
                      <i className="fa fa-trash"> Delete</i>
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ListProducts;
