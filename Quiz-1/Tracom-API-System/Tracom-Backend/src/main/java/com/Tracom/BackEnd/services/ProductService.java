package com.Tracom.BackEnd.services;

import java.util.List;

import com.Tracom.BackEnd.entities.Product;

public interface ProductService {

	List<Product> getAllProduct();

	void saveProduct(Product product);

	Product findProductById(String id);

	void updateProduct(Product product);
	
	Product deleteProductById(String id);
	
	

}
