package arraysuniquedupnumbersorder;

import java.util.ArrayList;
import java.util.Arrays;

public class ArraysUniqueDupNumbersOrder {
    public static void main(String args[]) {
       int[]   Male = { 17,19,20,22,27,45,56,59,69 };
       int[] Female = {17,22,25,29,32,34,35,45,59 };
        
        ArrayList<Integer> unique = new ArrayList<>();
        ArrayList<Integer> dup = new ArrayList<>();

        // get the duplicate (common) numbers
        // get the unique numbers of array1 when compared to array2
        for(int i=0; i<Male.length; i++){
            boolean duplicate = false;
            for(int j=0; j<Male.length; j++){
                if(Male[i] == Female[j]){
                    dup.add(Male[i]);
                    duplicate = true;
                    break;
                }
            }
            if (!duplicate) {
                unique.add(Male[i]);
            }
        }

        System.out.println("Duplicates are age for Tracom Employees Are: "+dup);

    }

}