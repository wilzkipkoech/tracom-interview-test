
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamasking;

public class DataMasking {

    public static void main(String[] args) throws Exception {

        System.out.println( maskString("1234-1234-1234-1234"));
        System.out.println( maskString("4556364607935616"));
        System.out.println( maskString("4556-3646-0793-5616"));
        System.out.println( maskString("ABCD-EFGH-IJKLM-NOPQ"));
        System.out.println( maskString(""));
        System.out.println( maskString("Skippy"));

    }

    private static String maskString(String strText)
            throws Exception{

        if(strText == null || strText.equals(""))
            return strText;

        String midString = strText.length() > 8 ? strText.substring(4, strText.length() - 4) : "";
        if(midString.length() == 0)
            return strText;

        String lastFour = strText.length() > 4 ? strText.substring(strText.length() - 4) : strText;
        String firstFour = strText.length() > 4 ? strText.substring(0,4) : strText;

        String regex = "[0-9]";

        String maskedMid = midString.replaceAll(regex, "#");

        return firstFour
                + maskedMid
                + lastFour;
    }
}