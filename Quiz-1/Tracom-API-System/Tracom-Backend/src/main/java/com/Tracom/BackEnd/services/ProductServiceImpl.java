package com.Tracom.BackEnd.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.Tracom.BackEnd.entities.Product;
import com.Tracom.BackEnd.repositories.ProductRepositories;



public class ProductServiceImpl implements ProductService  {

	@Autowired
	ProductRepositories productrepository;
	
	@Override
	public List<Product> getAllProduct() {
		return productrepository.findAll();
	}
	
	@Override
	public void saveProduct(Product product) {
		productrepository.save(product);
	}
	
    @Override
	public Product findProductById(String id) {
		return productrepository.findByproductId(id);
	}
    
	public Product deleteProductById(String id) {
		productrepository.deleteById(id);
		 return null;
	}
	
	@Override
	public void updateProduct(Product product) {
		productrepository.save(product);		
	}

}
