package com.Tracom.BackEnd.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.Tracom.BackEnd.entities.Product;
import com.Tracom.BackEnd.services.ProductService;
import com.Tracom.BackEnd.services.ProductServiceImpl;

@CrossOrigin(origins = { "http://localhost:3000","http://localhost:3001", "http://localhost:4200" })
@RestController

@Configuration
@Import({ProductServiceImpl.class})
public class ProductController {
	
	@Autowired
	ProductService productservice;
	
	
	@PostMapping("/post/product")
	 public HttpStatus createProduct(@Validated @RequestBody Product product) {
		productservice.saveProduct(product);
		return HttpStatus.CREATED;
	}
	
	
	@GetMapping("/product/all")
	public List<Product> findAll(){
		return productservice.getAllProduct();
	}
	
	
	
	@GetMapping("/get/product/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable("id") String id){
		return new ResponseEntity<Product>( productservice.findProductById(id), HttpStatus.OK);
	}
	
	
	
	@PutMapping("/update/product")
	public ResponseEntity <Product> updateProduct(@Valid @RequestBody Product product) {
		 productservice.updateProduct(product);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}
	
	
	@DeleteMapping("/delete/product/{id}")
	public ResponseEntity<Product> deleteProduct(@PathVariable("id") String id) {
		productservice.deleteProductById(id);
		return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
	}
	
	

}
