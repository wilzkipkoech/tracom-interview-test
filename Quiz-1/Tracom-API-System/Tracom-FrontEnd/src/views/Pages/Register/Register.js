import React, { Component } from 'react';
import axios from "axios";
import { Formik,  Field } from "formik";
import { Button, Card, CardBody, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

class Register extends Component {

  constructor(props) {
    super(props);
    this.state = { books: [], name: null };
    this.state = { password: "", username: "",email: ""};
    this.handleChangepassword = this.handleChangepassword.bind(this);
    this.handleChangename = this.handleChangename.bind(this);
    this.handleChangeemail = this.handleChangeemail.bind(this);




    this.onSubmit = this.onSubmit.bind(this);
    this.routeListUser = this.routeListUser.bind(this);
  }



  //GET username METHOD
  handleChangename(f) {
    this.setState({
      username: f.target.value
    });
  }


  //GET Email METHOD
  handleChangeemail(i) {
    this.setState({
      email: i.target.value
    });
  }

    //GET Password METHOD
    handleChangepassword(e) {
      this.setState({
        password: e.target.value
      });
    }

     //GET Password METHOD
     handleChangepassword(e) {
      this.setState({
        password: e.target.value
      });
    }



  //ON SUBMIT FORM METHOD
  onSubmit(e) {
    e.preventDefault();
    const save = {
      password: this.state.password,
      username: this.state.username,
      email: this.state.email,


    };
    axios.post("http://localhost:8000/user", save).then(res => {
      if (res.status === 200) {
        // alert("Product Added successfully.!");
        window.location.reload();
      }
    });

    this.setState({
      password: "",
      username: "",
      email: "",

    });

    this.routeListUser();
  }

  //BACK FUNCTION TO BOOK lIST
  routeListUser() {
    let path = `/dashboard`;
    this.props.history.push(path);
  }



  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4">
                <CardBody className="p-4">

        <Formik>
          <Form className="container" onSubmit={this.onSubmit}>


          <fieldset className="form-group">
              <label>User Full Names </label>
              <Field
                className="form-control"
                type="text"
                name="email"
                required
                value={this.state.email}
                onChange={this.handleChangeemail}
                placeholder=" Full NameHere"
              />
            </fieldset>

            <fieldset className="form-group">
              <label>E-mail</label>
              <Field
                className="form-control"
                type="email"
                name="username"
                required
                value={this.state.username}
                onChange={this.handleChangename}
                placeholder="Email Here"
              />
            </fieldset>
            <fieldset>
              <label>Password</label>
              <Field
                className="form-control"
                type="password"
                name="password"
                required
                value={this.state.password}
                onChange={this.handleChangepassword}
                placeholder="Password"

              />
            </fieldset>

            <fieldset>
              <label> Confirm Password</label>
              <Field
                className="form-control"
                type="password"
                name="password"
                required
                value={this.state.password}
                onChange={this.handleChangepassword}
                placeholder=" Confirm Password"

              />
            </fieldset>


<br></br>

            &nbsp;
            <Button className="btn btn-success"
              value="Submit"
              type="submit" block><i className="fa fa-plus"> Register Account</i></Button>


          </Form>
        </Formik>
                </CardBody>

              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Register;
