import React, { Component } from "react";
import axios from "axios";
import { Formik, Form, Field } from "formik";
class EditProduct extends Component {
  constructor(props) {
    super(props);
    this.state = { products: [], name: null };
    this.state.products = {
      productId:this.props.match.params.id,
       productName: "",
       productDescription: "",
        productPrice: "",
        productCategory: "",
        productCode: ""
      };
    this.handleChangeid = this.handleChangeid.bind(this);
    this.handleChangename = this.handleChangename.bind(this);
    this.handleChangecode = this.handleChangecode.bind(this);
    this.handleChangecategory = this.handleChangecategory.bind(this);
    this.handleChangeprice = this.handleChangeprice.bind(this);
    this.handleChangedescription = this.handleChangedescription.bind(this);



    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    axios
      .get(
        "http://localhost:8080/update/product" + this.props.match.params.id
      )
      .then(result => {
        console.table(result);
        this.setState({
          productId: result.data.productid,
          productName: result.data.productname,
          productCode: result.data.productcode,
          productCategory: result.data.productcategory,
          productDescription: result.data.productdescription,
          productPrice: result.data.productprice
        });
      });
  }
//GET product id METHOD
handleChangeid(e) {
  this.setState({
    productid: e.target.value
  });
}

//GET productname METHOD
handleChangename(f) {
  this.setState({
    productname: f.target.value
  });
}

//GET description METHOD
handleChangedescription(g) {
  this.setState({
    productdescription: g.target.value
  });
}

//GET price METHOD
handleChangeprice(h) {
  this.setState({
    productprice: h.target.value
  });
}

//GET code METHOD
handleChangecode(i) {
  this.setState({
    productcode: i.target.value
  });
}

//GET category METHOD
handleChangecategory(j) {
  this.setState({
    productcategory: j.target.value
  });
}

//ON SUBMIT FORM METHOD
onSubmit(e) {
  e.preventDefault();
  const update = {
    productId: this.state.productid,
    productName: this.state.productname,
    productCode: this.state.productcode,
    productCategory: this.state.productcategory,
    productDescription: this.state.productdescription,
    productPrice: this.state.productprice

  };
    axios.put("http://localhost:8080/update/product", update).then(res => {
      if (res.status === 200) {
        alert("Product update successfully.");
        window.location.reload();
      }
    });

    this.routeListProduct();
  }

  //BACK Product LIST
  routeListProduct() {
    let path = '/dashboard';
    this.props.history.push(this.path);
  }

  render() {
    return (
      <div className="col-sm-12">
        <div className="container">
          <br />

          <h3 align="center">Edit Products</h3>
        </div>

        <Formik>
          <Form className="container" onSubmit={this.onSubmit}>

          <fieldset>
              <label>Product Id</label>
              <Field
                className="form-control"
                type="text"
                name="txtid"
                value={this.state.productid}
                onChange={this.handleChangeid}
              placeholder="Product ID"

              />
            </fieldset>
          <fieldset className="form-group">
              <label>Product Name</label>
              <Field
                className="form-control"
                type="text"
                name="productname"
                value={this.state.productname}
                onChange={this.handleChangename}
                placeholder="Product Name Here"
              />
            </fieldset>
            <fieldset className="form-group">
              <label>Product Code </label>
              <Field
                className="form-control"
                type="text"
                name="productdescription"
                value={this.state.productcode}
                onChange={this.handleChangecode}
                placeholder="Product Code Here"
              />
            </fieldset>
            <fieldset className="form-group">
              <label>Product Category </label>
              <Field
                className="form-control"
                type="text"
                name="productcategory"
                value={this.state.productcategory}
                onChange={this.handleChangecategory}
                placeholder="Produc Category  Here"
              />
            </fieldset>
            <fieldset className="form-group">
              <label>Product Description </label>
              <Field
                className="form-control"
                type="text"
                name="productdescription"
                value={this.state.productdescription}
                onChange={this.handleChangedescription}
                placeholder="Product Category Here"
              />
            </fieldset>

            <fieldset className="form-group">
              <label>Product Price</label>
              <Field
                className="form-control"
                type="text"
                name="productprice"
                value={this.state.productprice}
                onChange={this.handleChangeprice}
                placeholder="Product Price Here"
              />
            </fieldset>

            <button
              className="btn btn-success"
              value="Submit"
              type="submit"
              align="center"
            >
              <i className="fa fa-plus"> Update</i>
            </button>
            &nbsp;
            <button
              className="btn btn-danger"
              type="reset"
              onClick={this.routeListProduct}
              align="center"
            >
              <i className="fa fa-location-arrow"> cancel</i>
            </button>
            <br />
            &nbsp; &nbsp; &nbsp;
          </Form>
        </Formik>
      </div>
    );
  }
}

export default EditProduct;
